//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.util.*;

class GFG
{
    public static void main(String args[])throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while(t-- > 0)
        {
            String s = read.readLine().trim();

            Solution ob = new Solution();
            System.out.println(ob.longestUniqueSubsttr(s));
        }
    }
}
// } Driver Code Ends


class Solution{
    int longestUniqueSubsttr(String S){
        if (S == null || S.length() == 0) {
            return 0;
        }
        
        int maxLength = 0;
        Map<Character, Integer> charIndexMap = new HashMap<>();
        
        for (int start = 0, end = 0; end < S.length(); end++) {
            char currentChar = S.charAt(end);
            if (charIndexMap.containsKey(currentChar)) {
                // If the current character is already in the map, move the start pointer to the next position after the last occurrence of this character.
                start = Math.max(start, charIndexMap.get(currentChar) + 1);
            }
            // Update the maximum length of substring without repeating characters.
            maxLength = Math.max(maxLength, end - start + 1);
            // Update the index of the current character.
            charIndexMap.put(currentChar, end);
        }
        
        return maxLength;
    }
}
