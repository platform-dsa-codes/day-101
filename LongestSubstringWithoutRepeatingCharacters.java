import java.util.*;

class Solution {
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        
        int maxLength = 0;
        Map<Character, Integer> charIndexMap = new HashMap<>();
        
        for (int start = 0, end = 0; end < s.length(); end++) {
            char currentChar = s.charAt(end);
            if (charIndexMap.containsKey(currentChar)) {
                // If the current character is already in the map, move the start pointer to the next position after the last occurrence of this character.
                start = Math.max(start, charIndexMap.get(currentChar) + 1);
            }
            // Update the maximum length of substring without repeating characters.
            maxLength = Math.max(maxLength, end - start + 1);
            // Update the index of the current character.
            charIndexMap.put(currentChar, end);
        }
        
        return maxLength;
    }
}
